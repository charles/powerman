/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison implementation for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

/* C LALR(1) parser skeleton written by Richard Stallman, by
   simplifying the original so-called "semantic" parser.  */

/* All symbols defined below should begin with yy or YY, to avoid
   infringing on user name space.  This should be done even for local
   variables, as they might otherwise be expanded by user macros.
   There are some unavoidable exceptions within include files to
   define necessary library symbols; they are noted "INFRINGES ON
   USER NAME SPACE" below.  */

/* Identify Bison output.  */
#define YYBISON 1

/* Bison version.  */
#define YYBISON_VERSION "3.0.4"

/* Skeleton name.  */
#define YYSKELETON_NAME "yacc.c"

/* Pure parsers.  */
#define YYPURE 0

/* Push parsers.  */
#define YYPUSH 0

/* Pull parsers.  */
#define YYPULL 1




/* Copy the first part of user declarations.  */
#line 25 "parse_tab.y" /* yacc.c:339  */

#if HAVE_CONFIG_H
#include "config.h"
#endif
#define YYSTYPE char *  /*  The generic type returned by all parse matches */
#undef YYDEBUG          /* no debug code plese */
#include <sys/stat.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>       /* for HUGE_VAL and trunc */
#include <assert.h>
#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <limits.h>
#include <string.h>
#include <sys/time.h>

#include "list.h"
#include "cbuf.h"
#include "hostlist.h"
#include "xtypes.h"
#include "xmalloc.h"
#include "xpoll.h"
#include "xregex.h"
#include "pluglist.h"
#include "arglist.h"
#include "device_private.h"
#include "device_serial.h"
#include "device_pipe.h"
#include "device_tcp.h"
#include "parse_util.h"
#include "error.h"

/*
 * A PreScript is a list of PreStmts.
 */
#define PRESTMT_MAGIC 0x89786756
typedef struct {
    int magic;
    StmtType type;              /* delay/expect/send */
    char *str;                  /* expect string, send fmt, setplugstate plug */
    struct timeval tv;          /* delay value */
    int mp1;                    /* setplugstate plug match position */
    int mp2;                    /* setplugstate state match position */
    List prestmts;              /* subblock */
    List interps;               /* interpretations for setplugstate */
} PreStmt;
typedef List PreScript;

/*
 * Unprocessed Protocol (used during parsing).
 * This data will be copied for each instantiation of a device.
 */
typedef struct {
    char *name;                 /* specification name, e.g. "icebox" */
    struct timeval timeout;     /* timeout for this device */
    struct timeval ping_period; /* ping period for this device 0.0 = none */
    List plugs;                 /* list of plug names (e.g. "1" thru "10") */
    PreScript prescripts[NUM_SCRIPTS];  /* array of PreScripts */
} Spec;                                 /*   script may be NULL if undefined */

/* powerman.conf */
static void makeNode(char *nodestr, char *devstr, char *plugstr);
static void makeAlias(char *namestr, char *hostsstr);
static Stmt *makeStmt(PreStmt *p);
static void destroyStmt(Stmt *stmt);
static void makeDevice(char *devstr, char *specstr, char *hoststr, 
                        char *portstr);

/* device config */
static PreStmt *makePreStmt(StmtType type, char *str, char *tvstr, 
                      char *mp1str, char *mp2str, List prestmts, List interps);
static void destroyPreStmt(PreStmt *p);
static Spec *makeSpec(char *name);
static Spec *findSpec(char *name);
static int matchSpec(Spec * spec, void *key);
static void destroySpec(Spec * spec);
static void _clear_current_spec(void);
static void makeScript(int com, List stmts);
static void destroyInterp(Interp *i);
static Interp *makeInterp(InterpState state, char *str);
static List copyInterpList(List ilist);

/* utility functions */
static void _errormsg(char *msg);
static void _warnmsg(char *msg);
static long _strtolong(char *str);
static double _strtodouble(char *str);
static void _doubletotv(struct timeval *tv, double val);

extern int yylex();
void yyerror();


static List device_specs = NULL;      /* list of Spec's */
static Spec current_spec;             /* Holds a Spec as it is built */

#line 166 "parse_tab.c" /* yacc.c:339  */

# ifndef YY_NULLPTR
#  if defined __cplusplus && 201103L <= __cplusplus
#   define YY_NULLPTR nullptr
#  else
#   define YY_NULLPTR 0
#  endif
# endif

/* Enabling verbose error messages.  */
#ifdef YYERROR_VERBOSE
# undef YYERROR_VERBOSE
# define YYERROR_VERBOSE 1
#else
# define YYERROR_VERBOSE 0
#endif

/* In a future release of Bison, this section will be replaced
   by #include "y.tab.h".  */
#ifndef YY_YY_PARSE_TAB_H_INCLUDED
# define YY_YY_PARSE_TAB_H_INCLUDED
/* Debug traces.  */
#ifndef YYDEBUG
# define YYDEBUG 0
#endif
#if YYDEBUG
extern int yydebug;
#endif

/* Token type.  */
#ifndef YYTOKENTYPE
# define YYTOKENTYPE
  enum yytokentype
  {
    TOK_LOGIN = 258,
    TOK_LOGOUT = 259,
    TOK_STATUS = 260,
    TOK_STATUS_ALL = 261,
    TOK_STATUS_TEMP = 262,
    TOK_STATUS_TEMP_ALL = 263,
    TOK_STATUS_BEACON = 264,
    TOK_STATUS_BEACON_ALL = 265,
    TOK_BEACON_ON = 266,
    TOK_BEACON_ON_RANGED = 267,
    TOK_BEACON_OFF = 268,
    TOK_BEACON_OFF_RANGED = 269,
    TOK_ON = 270,
    TOK_ON_RANGED = 271,
    TOK_ON_ALL = 272,
    TOK_OFF = 273,
    TOK_OFF_RANGED = 274,
    TOK_OFF_ALL = 275,
    TOK_CYCLE = 276,
    TOK_CYCLE_RANGED = 277,
    TOK_CYCLE_ALL = 278,
    TOK_RESET = 279,
    TOK_RESET_RANGED = 280,
    TOK_RESET_ALL = 281,
    TOK_PING = 282,
    TOK_SPEC = 283,
    TOK_EXPECT = 284,
    TOK_SETPLUGSTATE = 285,
    TOK_SEND = 286,
    TOK_DELAY = 287,
    TOK_FOREACHPLUG = 288,
    TOK_FOREACHNODE = 289,
    TOK_IFOFF = 290,
    TOK_IFON = 291,
    TOK_OFF_STRING = 292,
    TOK_ON_STRING = 293,
    TOK_MAX_PLUG_COUNT = 294,
    TOK_TIMEOUT = 295,
    TOK_DEV_TIMEOUT = 296,
    TOK_PING_PERIOD = 297,
    TOK_PLUG_NAME = 298,
    TOK_SCRIPT = 299,
    TOK_DEVICE = 300,
    TOK_NODE = 301,
    TOK_ALIAS = 302,
    TOK_TCP_WRAPPERS = 303,
    TOK_LISTEN = 304,
    TOK_PLUG_LOG_LEVEL = 305,
    TOK_MATCHPOS = 306,
    TOK_STRING_VAL = 307,
    TOK_NUMERIC_VAL = 308,
    TOK_YES = 309,
    TOK_NO = 310,
    TOK_BEGIN = 311,
    TOK_END = 312,
    TOK_UNRECOGNIZED = 313,
    TOK_EQUALS = 314
  };
#endif
/* Tokens.  */
#define TOK_LOGIN 258
#define TOK_LOGOUT 259
#define TOK_STATUS 260
#define TOK_STATUS_ALL 261
#define TOK_STATUS_TEMP 262
#define TOK_STATUS_TEMP_ALL 263
#define TOK_STATUS_BEACON 264
#define TOK_STATUS_BEACON_ALL 265
#define TOK_BEACON_ON 266
#define TOK_BEACON_ON_RANGED 267
#define TOK_BEACON_OFF 268
#define TOK_BEACON_OFF_RANGED 269
#define TOK_ON 270
#define TOK_ON_RANGED 271
#define TOK_ON_ALL 272
#define TOK_OFF 273
#define TOK_OFF_RANGED 274
#define TOK_OFF_ALL 275
#define TOK_CYCLE 276
#define TOK_CYCLE_RANGED 277
#define TOK_CYCLE_ALL 278
#define TOK_RESET 279
#define TOK_RESET_RANGED 280
#define TOK_RESET_ALL 281
#define TOK_PING 282
#define TOK_SPEC 283
#define TOK_EXPECT 284
#define TOK_SETPLUGSTATE 285
#define TOK_SEND 286
#define TOK_DELAY 287
#define TOK_FOREACHPLUG 288
#define TOK_FOREACHNODE 289
#define TOK_IFOFF 290
#define TOK_IFON 291
#define TOK_OFF_STRING 292
#define TOK_ON_STRING 293
#define TOK_MAX_PLUG_COUNT 294
#define TOK_TIMEOUT 295
#define TOK_DEV_TIMEOUT 296
#define TOK_PING_PERIOD 297
#define TOK_PLUG_NAME 298
#define TOK_SCRIPT 299
#define TOK_DEVICE 300
#define TOK_NODE 301
#define TOK_ALIAS 302
#define TOK_TCP_WRAPPERS 303
#define TOK_LISTEN 304
#define TOK_PLUG_LOG_LEVEL 305
#define TOK_MATCHPOS 306
#define TOK_STRING_VAL 307
#define TOK_NUMERIC_VAL 308
#define TOK_YES 309
#define TOK_NO 310
#define TOK_BEGIN 311
#define TOK_END 312
#define TOK_UNRECOGNIZED 313
#define TOK_EQUALS 314

/* Value type.  */
#if ! defined YYSTYPE && ! defined YYSTYPE_IS_DECLARED
typedef int YYSTYPE;
# define YYSTYPE_IS_TRIVIAL 1
# define YYSTYPE_IS_DECLARED 1
#endif


extern YYSTYPE yylval;

int yyparse (void);

#endif /* !YY_YY_PARSE_TAB_H_INCLUDED  */

/* Copy the second part of user declarations.  */

#line 335 "parse_tab.c" /* yacc.c:358  */

#ifdef short
# undef short
#endif

#ifdef YYTYPE_UINT8
typedef YYTYPE_UINT8 yytype_uint8;
#else
typedef unsigned char yytype_uint8;
#endif

#ifdef YYTYPE_INT8
typedef YYTYPE_INT8 yytype_int8;
#else
typedef signed char yytype_int8;
#endif

#ifdef YYTYPE_UINT16
typedef YYTYPE_UINT16 yytype_uint16;
#else
typedef unsigned short int yytype_uint16;
#endif

#ifdef YYTYPE_INT16
typedef YYTYPE_INT16 yytype_int16;
#else
typedef short int yytype_int16;
#endif

#ifndef YYSIZE_T
# ifdef __SIZE_TYPE__
#  define YYSIZE_T __SIZE_TYPE__
# elif defined size_t
#  define YYSIZE_T size_t
# elif ! defined YYSIZE_T
#  include <stddef.h> /* INFRINGES ON USER NAME SPACE */
#  define YYSIZE_T size_t
# else
#  define YYSIZE_T unsigned int
# endif
#endif

#define YYSIZE_MAXIMUM ((YYSIZE_T) -1)

#ifndef YY_
# if defined YYENABLE_NLS && YYENABLE_NLS
#  if ENABLE_NLS
#   include <libintl.h> /* INFRINGES ON USER NAME SPACE */
#   define YY_(Msgid) dgettext ("bison-runtime", Msgid)
#  endif
# endif
# ifndef YY_
#  define YY_(Msgid) Msgid
# endif
#endif

#ifndef YY_ATTRIBUTE
# if (defined __GNUC__                                               \
      && (2 < __GNUC__ || (__GNUC__ == 2 && 96 <= __GNUC_MINOR__)))  \
     || defined __SUNPRO_C && 0x5110 <= __SUNPRO_C
#  define YY_ATTRIBUTE(Spec) __attribute__(Spec)
# else
#  define YY_ATTRIBUTE(Spec) /* empty */
# endif
#endif

#ifndef YY_ATTRIBUTE_PURE
# define YY_ATTRIBUTE_PURE   YY_ATTRIBUTE ((__pure__))
#endif

#ifndef YY_ATTRIBUTE_UNUSED
# define YY_ATTRIBUTE_UNUSED YY_ATTRIBUTE ((__unused__))
#endif

#if !defined _Noreturn \
     && (!defined __STDC_VERSION__ || __STDC_VERSION__ < 201112)
# if defined _MSC_VER && 1200 <= _MSC_VER
#  define _Noreturn __declspec (noreturn)
# else
#  define _Noreturn YY_ATTRIBUTE ((__noreturn__))
# endif
#endif

/* Suppress unused-variable warnings by "using" E.  */
#if ! defined lint || defined __GNUC__
# define YYUSE(E) ((void) (E))
#else
# define YYUSE(E) /* empty */
#endif

#if defined __GNUC__ && 407 <= __GNUC__ * 100 + __GNUC_MINOR__
/* Suppress an incorrect diagnostic about yylval being uninitialized.  */
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN \
    _Pragma ("GCC diagnostic push") \
    _Pragma ("GCC diagnostic ignored \"-Wuninitialized\"")\
    _Pragma ("GCC diagnostic ignored \"-Wmaybe-uninitialized\"")
# define YY_IGNORE_MAYBE_UNINITIALIZED_END \
    _Pragma ("GCC diagnostic pop")
#else
# define YY_INITIAL_VALUE(Value) Value
#endif
#ifndef YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
# define YY_IGNORE_MAYBE_UNINITIALIZED_END
#endif
#ifndef YY_INITIAL_VALUE
# define YY_INITIAL_VALUE(Value) /* Nothing. */
#endif


#if ! defined yyoverflow || YYERROR_VERBOSE

/* The parser invokes alloca or malloc; define the necessary symbols.  */

# ifdef YYSTACK_USE_ALLOCA
#  if YYSTACK_USE_ALLOCA
#   ifdef __GNUC__
#    define YYSTACK_ALLOC __builtin_alloca
#   elif defined __BUILTIN_VA_ARG_INCR
#    include <alloca.h> /* INFRINGES ON USER NAME SPACE */
#   elif defined _AIX
#    define YYSTACK_ALLOC __alloca
#   elif defined _MSC_VER
#    include <malloc.h> /* INFRINGES ON USER NAME SPACE */
#    define alloca _alloca
#   else
#    define YYSTACK_ALLOC alloca
#    if ! defined _ALLOCA_H && ! defined EXIT_SUCCESS
#     include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
      /* Use EXIT_SUCCESS as a witness for stdlib.h.  */
#     ifndef EXIT_SUCCESS
#      define EXIT_SUCCESS 0
#     endif
#    endif
#   endif
#  endif
# endif

# ifdef YYSTACK_ALLOC
   /* Pacify GCC's 'empty if-body' warning.  */
#  define YYSTACK_FREE(Ptr) do { /* empty */; } while (0)
#  ifndef YYSTACK_ALLOC_MAXIMUM
    /* The OS might guarantee only one guard page at the bottom of the stack,
       and a page size can be as small as 4096 bytes.  So we cannot safely
       invoke alloca (N) if N exceeds 4096.  Use a slightly smaller number
       to allow for a few compiler-allocated temporary stack slots.  */
#   define YYSTACK_ALLOC_MAXIMUM 4032 /* reasonable circa 2006 */
#  endif
# else
#  define YYSTACK_ALLOC YYMALLOC
#  define YYSTACK_FREE YYFREE
#  ifndef YYSTACK_ALLOC_MAXIMUM
#   define YYSTACK_ALLOC_MAXIMUM YYSIZE_MAXIMUM
#  endif
#  if (defined __cplusplus && ! defined EXIT_SUCCESS \
       && ! ((defined YYMALLOC || defined malloc) \
             && (defined YYFREE || defined free)))
#   include <stdlib.h> /* INFRINGES ON USER NAME SPACE */
#   ifndef EXIT_SUCCESS
#    define EXIT_SUCCESS 0
#   endif
#  endif
#  ifndef YYMALLOC
#   define YYMALLOC malloc
#   if ! defined malloc && ! defined EXIT_SUCCESS
void *malloc (YYSIZE_T); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
#  ifndef YYFREE
#   define YYFREE free
#   if ! defined free && ! defined EXIT_SUCCESS
void free (void *); /* INFRINGES ON USER NAME SPACE */
#   endif
#  endif
# endif
#endif /* ! defined yyoverflow || YYERROR_VERBOSE */


#if (! defined yyoverflow \
     && (! defined __cplusplus \
         || (defined YYSTYPE_IS_TRIVIAL && YYSTYPE_IS_TRIVIAL)))

/* A type that is properly aligned for any stack member.  */
union yyalloc
{
  yytype_int16 yyss_alloc;
  YYSTYPE yyvs_alloc;
};

/* The size of the maximum gap between one aligned stack and the next.  */
# define YYSTACK_GAP_MAXIMUM (sizeof (union yyalloc) - 1)

/* The size of an array large to enough to hold all stacks, each with
   N elements.  */
# define YYSTACK_BYTES(N) \
     ((N) * (sizeof (yytype_int16) + sizeof (YYSTYPE)) \
      + YYSTACK_GAP_MAXIMUM)

# define YYCOPY_NEEDED 1

/* Relocate STACK from its old location to the new one.  The
   local variables YYSIZE and YYSTACKSIZE give the old and new number of
   elements in the stack, and YYPTR gives the new location of the
   stack.  Advance YYPTR to a properly aligned location for the next
   stack.  */
# define YYSTACK_RELOCATE(Stack_alloc, Stack)                           \
    do                                                                  \
      {                                                                 \
        YYSIZE_T yynewbytes;                                            \
        YYCOPY (&yyptr->Stack_alloc, Stack, yysize);                    \
        Stack = &yyptr->Stack_alloc;                                    \
        yynewbytes = yystacksize * sizeof (*Stack) + YYSTACK_GAP_MAXIMUM; \
        yyptr += yynewbytes / sizeof (*yyptr);                          \
      }                                                                 \
    while (0)

#endif

#if defined YYCOPY_NEEDED && YYCOPY_NEEDED
/* Copy COUNT objects from SRC to DST.  The source and destination do
   not overlap.  */
# ifndef YYCOPY
#  if defined __GNUC__ && 1 < __GNUC__
#   define YYCOPY(Dst, Src, Count) \
      __builtin_memcpy (Dst, Src, (Count) * sizeof (*(Src)))
#  else
#   define YYCOPY(Dst, Src, Count)              \
      do                                        \
        {                                       \
          YYSIZE_T yyi;                         \
          for (yyi = 0; yyi < (Count); yyi++)   \
            (Dst)[yyi] = (Src)[yyi];            \
        }                                       \
      while (0)
#  endif
# endif
#endif /* !YYCOPY_NEEDED */

/* YYFINAL -- State number of the termination state.  */
#define YYFINAL  26
/* YYLAST -- Last index in YYTABLE.  */
#define YYLAST   137

/* YYNTOKENS -- Number of terminals.  */
#define YYNTOKENS  60
/* YYNNTS -- Number of nonterminals.  */
#define YYNNTS  25
/* YYNRULES -- Number of rules.  */
#define YYNRULES  82
/* YYNSTATES -- Number of states.  */
#define YYNSTATES  143

/* YYTRANSLATE[YYX] -- Symbol number corresponding to YYX as returned
   by yylex, with out-of-bounds checking.  */
#define YYUNDEFTOK  2
#define YYMAXUTOK   314

#define YYTRANSLATE(YYX)                                                \
  ((unsigned int) (YYX) <= YYMAXUTOK ? yytranslate[YYX] : YYUNDEFTOK)

/* YYTRANSLATE[TOKEN-NUM] -- Symbol number corresponding to TOKEN-NUM
   as returned by yylex, without out-of-bounds checking.  */
static const yytype_uint8 yytranslate[] =
{
       0,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     2,     2,     2,     2,
       2,     2,     2,     2,     2,     2,     1,     2,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    28,    29,    30,    31,    32,    33,    34,
      35,    36,    37,    38,    39,    40,    41,    42,    43,    44,
      45,    46,    47,    48,    49,    50,    51,    52,    53,    54,
      55,    56,    57,    58,    59
};

#if YYDEBUG
  /* YYRLINE[YYN] -- Source line where rule number YYN was defined.  */
static const yytype_uint16 yyrline[] =
{
       0,   153,   153,   159,   160,   161,   163,   164,   165,   166,
     167,   168,   169,   171,   174,   176,   180,   184,   188,   191,
     195,   197,   201,   208,   214,   215,   217,   218,   219,   220,
     222,   226,   230,   233,   238,   244,   245,   247,   249,   251,
     253,   255,   257,   259,   261,   263,   265,   267,   269,   271,
     273,   275,   277,   279,   281,   283,   285,   287,   289,   291,
     293,   295,   299,   303,   306,   311,   313,   315,   317,   319,
     322,   324,   326,   328,   330,   333,   336,   339,   344,   347,
     352,   354,   358
};
#endif

#if YYDEBUG || YYERROR_VERBOSE || 0
/* YYTNAME[SYMBOL-NUM] -- String name of the symbol SYMBOL-NUM.
   First, the terminals, then, starting at YYNTOKENS, nonterminals.  */
static const char *const yytname[] =
{
  "$end", "error", "$undefined", "TOK_LOGIN", "TOK_LOGOUT", "TOK_STATUS",
  "TOK_STATUS_ALL", "TOK_STATUS_TEMP", "TOK_STATUS_TEMP_ALL",
  "TOK_STATUS_BEACON", "TOK_STATUS_BEACON_ALL", "TOK_BEACON_ON",
  "TOK_BEACON_ON_RANGED", "TOK_BEACON_OFF", "TOK_BEACON_OFF_RANGED",
  "TOK_ON", "TOK_ON_RANGED", "TOK_ON_ALL", "TOK_OFF", "TOK_OFF_RANGED",
  "TOK_OFF_ALL", "TOK_CYCLE", "TOK_CYCLE_RANGED", "TOK_CYCLE_ALL",
  "TOK_RESET", "TOK_RESET_RANGED", "TOK_RESET_ALL", "TOK_PING", "TOK_SPEC",
  "TOK_EXPECT", "TOK_SETPLUGSTATE", "TOK_SEND", "TOK_DELAY",
  "TOK_FOREACHPLUG", "TOK_FOREACHNODE", "TOK_IFOFF", "TOK_IFON",
  "TOK_OFF_STRING", "TOK_ON_STRING", "TOK_MAX_PLUG_COUNT", "TOK_TIMEOUT",
  "TOK_DEV_TIMEOUT", "TOK_PING_PERIOD", "TOK_PLUG_NAME", "TOK_SCRIPT",
  "TOK_DEVICE", "TOK_NODE", "TOK_ALIAS", "TOK_TCP_WRAPPERS", "TOK_LISTEN",
  "TOK_PLUG_LOG_LEVEL", "TOK_MATCHPOS", "TOK_STRING_VAL",
  "TOK_NUMERIC_VAL", "TOK_YES", "TOK_NO", "TOK_BEGIN", "TOK_END",
  "TOK_UNRECOGNIZED", "TOK_EQUALS", "$accept", "configuration_file",
  "config_list", "config_item", "TCP_wrappers", "plug_log_level", "listen",
  "device", "node", "alias", "spec", "spec_item_list", "spec_item",
  "spec_timeout", "spec_ping_period", "string_list", "spec_plug_list",
  "spec_script_list", "spec_script", "stmt_block", "stmt_list", "stmt",
  "interp_list", "interp", "regmatch", YY_NULLPTR
};
#endif

# ifdef YYPRINT
/* YYTOKNUM[NUM] -- (External) token number corresponding to the
   (internal) symbol number NUM (which must be that of a token).  */
static const yytype_uint16 yytoknum[] =
{
       0,   256,   257,   258,   259,   260,   261,   262,   263,   264,
     265,   266,   267,   268,   269,   270,   271,   272,   273,   274,
     275,   276,   277,   278,   279,   280,   281,   282,   283,   284,
     285,   286,   287,   288,   289,   290,   291,   292,   293,   294,
     295,   296,   297,   298,   299,   300,   301,   302,   303,   304,
     305,   306,   307,   308,   309,   310,   311,   312,   313,   314
};
# endif

#define YYPACT_NINF -86

#define yypact_value_is_default(Yystate) \
  (!!((Yystate) == (-86)))

#define YYTABLE_NINF -1

#define yytable_value_is_error(Yytable_value) \
  0

  /* YYPACT[STATE-NUM] -- Index in YYTABLE of the portion describing
     STATE-NUM.  */
static const yytype_int8 yypact[] =
{
      -3,   -26,    -2,     5,     7,   -14,    57,    59,    61,    -3,
     -86,   -86,   -86,   -86,   -86,   -86,   -86,   -86,    54,    60,
      64,    65,   -86,   -86,   -86,   -86,   -86,   -86,    58,    66,
      67,   -86,    68,    69,    70,    63,    -5,   -86,   -86,   -86,
     -86,    71,   -86,    72,   -86,   -86,   -86,    73,    74,    74,
      74,    74,    74,    74,    74,    74,    74,    74,    74,    74,
      74,    74,    74,    74,    74,    74,    74,    74,    74,    74,
      74,    74,    74,   -86,   -86,   -86,   -86,   -86,     1,    62,
     -86,   -86,   -86,   -86,   -86,   -86,   -86,   -86,   -86,   -86,
     -86,   -86,   -86,   -86,   -86,   -86,   -86,   -86,   -86,   -86,
     -86,   -86,   -86,   -86,   -86,   -86,   -86,    75,    53,    76,
      78,    74,    74,    74,    74,    -1,   -86,   -86,    79,    82,
       9,   -86,   -86,   -86,   -86,   -86,   -86,   -86,   -86,   -86,
      88,    55,    77,    88,   -86,    88,    88,    83,    85,   -86,
      88,   -86,   -86
};

  /* YYDEFACT[STATE-NUM] -- Default reduction number in state STATE-NUM.
     Performed when YYTABLE does not specify something else to do.  Zero
     means the default is an error.  */
static const yytype_uint8 yydefact[] =
{
       5,     0,     0,     0,     0,    13,     0,     0,     0,     2,
       4,     7,     8,     6,     9,    10,    11,    12,     0,     0,
       0,     0,    14,    15,    17,    16,     1,     3,     0,     0,
      21,    22,     0,     0,     0,     0,     0,    25,    26,    27,
      28,    29,    36,    19,    20,    30,    31,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,     0,     0,     0,     0,     0,     0,     0,
       0,     0,     0,    23,    24,    35,    18,    33,     0,     0,
      37,    38,    39,    40,    41,    42,    43,    44,    45,    46,
      47,    48,    49,    50,    51,    52,    53,    54,    55,    56,
      57,    58,    59,    60,    61,    32,    34,     0,     0,     0,
       0,     0,     0,     0,     0,     0,    64,    65,     0,     0,
      72,    66,    67,    75,    74,    76,    77,    62,    63,    82,
      68,     0,     0,    73,    79,    70,    69,     0,     0,    78,
      71,    80,    81
};

  /* YYPGOTO[NTERM-NUM].  */
static const yytype_int8 yypgoto[] =
{
     -86,   -86,   -86,   104,   -86,   -86,   -86,   -86,   -86,   -86,
     -86,   -86,    84,   -86,   -86,   -86,   -86,   -86,    93,   -49,
     -86,     8,   -81,   -85,   -12
};

  /* YYDEFGOTO[NTERM-NUM].  */
static const yytype_int16 yydefgoto[] =
{
      -1,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    36,    37,    38,    39,    78,    40,    41,    42,    80,
     115,   116,   133,   134,   120
};

  /* YYTABLE[YYPACT[STATE-NUM]] -- What to do in state STATE-NUM.  If
     positive, shift that token.  If negative, reduce the rule whose
     number is the opposite.  If YYTABLE_NINF, syntax error.  */
static const yytype_uint8 yytable[] =
{
      81,    82,    83,    84,    85,    86,    87,    88,    89,    90,
      91,    92,    93,    94,    95,    96,    97,    98,    99,   100,
     101,   102,   103,   104,   131,     1,    18,   132,   107,   108,
     109,   110,   111,   112,   113,   114,    32,    33,    34,    35,
      22,    23,     2,     3,     4,     5,     6,     7,   139,   136,
      19,   139,    73,   105,   140,   139,   127,    20,   106,    21,
     118,    26,   123,   124,   125,   126,    48,    49,    50,    51,
      52,    53,    54,    55,    56,    57,    58,    59,    60,    61,
      62,    63,    64,    65,    66,    67,    68,    69,    70,    71,
      72,   107,   108,   109,   110,   111,   112,   113,   114,    32,
      33,    34,    35,   131,   118,   119,   132,   130,   135,    24,
      28,    25,    29,    27,   137,    35,    30,    31,    43,    44,
      74,    45,    46,   128,    76,    77,    47,   117,   121,     0,
      79,   122,   129,   118,    75,   141,   138,   142
};

static const yytype_int16 yycheck[] =
{
      49,    50,    51,    52,    53,    54,    55,    56,    57,    58,
      59,    60,    61,    62,    63,    64,    65,    66,    67,    68,
      69,    70,    71,    72,    15,    28,    52,    18,    29,    30,
      31,    32,    33,    34,    35,    36,    41,    42,    43,    44,
      54,    55,    45,    46,    47,    48,    49,    50,   133,   130,
      52,   136,    57,    52,   135,   140,    57,    52,    57,    52,
      51,     0,   111,   112,   113,   114,     3,     4,     5,     6,
       7,     8,     9,    10,    11,    12,    13,    14,    15,    16,
      17,    18,    19,    20,    21,    22,    23,    24,    25,    26,
      27,    29,    30,    31,    32,    33,    34,    35,    36,    41,
      42,    43,    44,    15,    51,    52,    18,   119,   120,    52,
      56,    52,    52,     9,    59,    44,    52,    52,    52,    52,
      36,    53,    53,   115,    52,    52,    56,    52,    52,    -1,
      56,    53,    53,    51,    41,    52,    59,    52
};

  /* YYSTOS[STATE-NUM] -- The (internal number of the) accessing
     symbol of state STATE-NUM.  */
static const yytype_uint8 yystos[] =
{
       0,    28,    45,    46,    47,    48,    49,    50,    61,    62,
      63,    64,    65,    66,    67,    68,    69,    70,    52,    52,
      52,    52,    54,    55,    52,    52,     0,    63,    56,    52,
      52,    52,    41,    42,    43,    44,    71,    72,    73,    74,
      76,    77,    78,    52,    52,    53,    53,    56,     3,     4,
       5,     6,     7,     8,     9,    10,    11,    12,    13,    14,
      15,    16,    17,    18,    19,    20,    21,    22,    23,    24,
      25,    26,    27,    57,    72,    78,    52,    52,    75,    56,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    79,    79,    79,    79,    79,
      79,    79,    79,    79,    79,    52,    57,    29,    30,    31,
      32,    33,    34,    35,    36,    80,    81,    52,    51,    52,
      84,    52,    53,    79,    79,    79,    79,    57,    81,    53,
      84,    15,    18,    82,    83,    84,    82,    59,    59,    83,
      82,    52,    52
};

  /* YYR1[YYN] -- Symbol number of symbol that rule YYN derives.  */
static const yytype_uint8 yyr1[] =
{
       0,    60,    61,    62,    62,    62,    63,    63,    63,    63,
      63,    63,    63,    64,    64,    64,    65,    66,    67,    67,
      68,    68,    69,    70,    71,    71,    72,    72,    72,    72,
      73,    74,    75,    75,    76,    77,    77,    78,    78,    78,
      78,    78,    78,    78,    78,    78,    78,    78,    78,    78,
      78,    78,    78,    78,    78,    78,    78,    78,    78,    78,
      78,    78,    79,    80,    80,    81,    81,    81,    81,    81,
      81,    81,    81,    81,    81,    81,    81,    81,    82,    82,
      83,    83,    84
};

  /* YYR2[YYN] -- Number of symbols on the right hand side of rule YYN.  */
static const yytype_uint8 yyr2[] =
{
       0,     2,     1,     2,     1,     0,     1,     1,     1,     1,
       1,     1,     1,     1,     2,     2,     2,     2,     5,     4,
       4,     3,     3,     5,     2,     1,     1,     1,     1,     1,
       2,     2,     2,     1,     4,     2,     1,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     3,     3,     3,     3,     3,     3,     3,
       3,     3,     3,     2,     1,     2,     2,     2,     3,     4,
       3,     4,     2,     3,     2,     2,     2,     2,     2,     1,
       3,     3,     2
};


#define yyerrok         (yyerrstatus = 0)
#define yyclearin       (yychar = YYEMPTY)
#define YYEMPTY         (-2)
#define YYEOF           0

#define YYACCEPT        goto yyacceptlab
#define YYABORT         goto yyabortlab
#define YYERROR         goto yyerrorlab


#define YYRECOVERING()  (!!yyerrstatus)

#define YYBACKUP(Token, Value)                                  \
do                                                              \
  if (yychar == YYEMPTY)                                        \
    {                                                           \
      yychar = (Token);                                         \
      yylval = (Value);                                         \
      YYPOPSTACK (yylen);                                       \
      yystate = *yyssp;                                         \
      goto yybackup;                                            \
    }                                                           \
  else                                                          \
    {                                                           \
      yyerror (YY_("syntax error: cannot back up")); \
      YYERROR;                                                  \
    }                                                           \
while (0)

/* Error token number */
#define YYTERROR        1
#define YYERRCODE       256



/* Enable debugging if requested.  */
#if YYDEBUG

# ifndef YYFPRINTF
#  include <stdio.h> /* INFRINGES ON USER NAME SPACE */
#  define YYFPRINTF fprintf
# endif

# define YYDPRINTF(Args)                        \
do {                                            \
  if (yydebug)                                  \
    YYFPRINTF Args;                             \
} while (0)

/* This macro is provided for backward compatibility. */
#ifndef YY_LOCATION_PRINT
# define YY_LOCATION_PRINT(File, Loc) ((void) 0)
#endif


# define YY_SYMBOL_PRINT(Title, Type, Value, Location)                    \
do {                                                                      \
  if (yydebug)                                                            \
    {                                                                     \
      YYFPRINTF (stderr, "%s ", Title);                                   \
      yy_symbol_print (stderr,                                            \
                  Type, Value); \
      YYFPRINTF (stderr, "\n");                                           \
    }                                                                     \
} while (0)


/*----------------------------------------.
| Print this symbol's value on YYOUTPUT.  |
`----------------------------------------*/

static void
yy_symbol_value_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  FILE *yyo = yyoutput;
  YYUSE (yyo);
  if (!yyvaluep)
    return;
# ifdef YYPRINT
  if (yytype < YYNTOKENS)
    YYPRINT (yyoutput, yytoknum[yytype], *yyvaluep);
# endif
  YYUSE (yytype);
}


/*--------------------------------.
| Print this symbol on YYOUTPUT.  |
`--------------------------------*/

static void
yy_symbol_print (FILE *yyoutput, int yytype, YYSTYPE const * const yyvaluep)
{
  YYFPRINTF (yyoutput, "%s %s (",
             yytype < YYNTOKENS ? "token" : "nterm", yytname[yytype]);

  yy_symbol_value_print (yyoutput, yytype, yyvaluep);
  YYFPRINTF (yyoutput, ")");
}

/*------------------------------------------------------------------.
| yy_stack_print -- Print the state stack from its BOTTOM up to its |
| TOP (included).                                                   |
`------------------------------------------------------------------*/

static void
yy_stack_print (yytype_int16 *yybottom, yytype_int16 *yytop)
{
  YYFPRINTF (stderr, "Stack now");
  for (; yybottom <= yytop; yybottom++)
    {
      int yybot = *yybottom;
      YYFPRINTF (stderr, " %d", yybot);
    }
  YYFPRINTF (stderr, "\n");
}

# define YY_STACK_PRINT(Bottom, Top)                            \
do {                                                            \
  if (yydebug)                                                  \
    yy_stack_print ((Bottom), (Top));                           \
} while (0)


/*------------------------------------------------.
| Report that the YYRULE is going to be reduced.  |
`------------------------------------------------*/

static void
yy_reduce_print (yytype_int16 *yyssp, YYSTYPE *yyvsp, int yyrule)
{
  unsigned long int yylno = yyrline[yyrule];
  int yynrhs = yyr2[yyrule];
  int yyi;
  YYFPRINTF (stderr, "Reducing stack by rule %d (line %lu):\n",
             yyrule - 1, yylno);
  /* The symbols being reduced.  */
  for (yyi = 0; yyi < yynrhs; yyi++)
    {
      YYFPRINTF (stderr, "   $%d = ", yyi + 1);
      yy_symbol_print (stderr,
                       yystos[yyssp[yyi + 1 - yynrhs]],
                       &(yyvsp[(yyi + 1) - (yynrhs)])
                                              );
      YYFPRINTF (stderr, "\n");
    }
}

# define YY_REDUCE_PRINT(Rule)          \
do {                                    \
  if (yydebug)                          \
    yy_reduce_print (yyssp, yyvsp, Rule); \
} while (0)

/* Nonzero means print parse trace.  It is left uninitialized so that
   multiple parsers can coexist.  */
int yydebug;
#else /* !YYDEBUG */
# define YYDPRINTF(Args)
# define YY_SYMBOL_PRINT(Title, Type, Value, Location)
# define YY_STACK_PRINT(Bottom, Top)
# define YY_REDUCE_PRINT(Rule)
#endif /* !YYDEBUG */


/* YYINITDEPTH -- initial size of the parser's stacks.  */
#ifndef YYINITDEPTH
# define YYINITDEPTH 200
#endif

/* YYMAXDEPTH -- maximum size the stacks can grow to (effective only
   if the built-in stack extension method is used).

   Do not make this value too large; the results are undefined if
   YYSTACK_ALLOC_MAXIMUM < YYSTACK_BYTES (YYMAXDEPTH)
   evaluated with infinite-precision integer arithmetic.  */

#ifndef YYMAXDEPTH
# define YYMAXDEPTH 10000
#endif


#if YYERROR_VERBOSE

# ifndef yystrlen
#  if defined __GLIBC__ && defined _STRING_H
#   define yystrlen strlen
#  else
/* Return the length of YYSTR.  */
static YYSIZE_T
yystrlen (const char *yystr)
{
  YYSIZE_T yylen;
  for (yylen = 0; yystr[yylen]; yylen++)
    continue;
  return yylen;
}
#  endif
# endif

# ifndef yystpcpy
#  if defined __GLIBC__ && defined _STRING_H && defined _GNU_SOURCE
#   define yystpcpy stpcpy
#  else
/* Copy YYSRC to YYDEST, returning the address of the terminating '\0' in
   YYDEST.  */
static char *
yystpcpy (char *yydest, const char *yysrc)
{
  char *yyd = yydest;
  const char *yys = yysrc;

  while ((*yyd++ = *yys++) != '\0')
    continue;

  return yyd - 1;
}
#  endif
# endif

# ifndef yytnamerr
/* Copy to YYRES the contents of YYSTR after stripping away unnecessary
   quotes and backslashes, so that it's suitable for yyerror.  The
   heuristic is that double-quoting is unnecessary unless the string
   contains an apostrophe, a comma, or backslash (other than
   backslash-backslash).  YYSTR is taken from yytname.  If YYRES is
   null, do not copy; instead, return the length of what the result
   would have been.  */
static YYSIZE_T
yytnamerr (char *yyres, const char *yystr)
{
  if (*yystr == '"')
    {
      YYSIZE_T yyn = 0;
      char const *yyp = yystr;

      for (;;)
        switch (*++yyp)
          {
          case '\'':
          case ',':
            goto do_not_strip_quotes;

          case '\\':
            if (*++yyp != '\\')
              goto do_not_strip_quotes;
            /* Fall through.  */
          default:
            if (yyres)
              yyres[yyn] = *yyp;
            yyn++;
            break;

          case '"':
            if (yyres)
              yyres[yyn] = '\0';
            return yyn;
          }
    do_not_strip_quotes: ;
    }

  if (! yyres)
    return yystrlen (yystr);

  return yystpcpy (yyres, yystr) - yyres;
}
# endif

/* Copy into *YYMSG, which is of size *YYMSG_ALLOC, an error message
   about the unexpected token YYTOKEN for the state stack whose top is
   YYSSP.

   Return 0 if *YYMSG was successfully written.  Return 1 if *YYMSG is
   not large enough to hold the message.  In that case, also set
   *YYMSG_ALLOC to the required number of bytes.  Return 2 if the
   required number of bytes is too large to store.  */
static int
yysyntax_error (YYSIZE_T *yymsg_alloc, char **yymsg,
                yytype_int16 *yyssp, int yytoken)
{
  YYSIZE_T yysize0 = yytnamerr (YY_NULLPTR, yytname[yytoken]);
  YYSIZE_T yysize = yysize0;
  enum { YYERROR_VERBOSE_ARGS_MAXIMUM = 5 };
  /* Internationalized format string. */
  const char *yyformat = YY_NULLPTR;
  /* Arguments of yyformat. */
  char const *yyarg[YYERROR_VERBOSE_ARGS_MAXIMUM];
  /* Number of reported tokens (one for the "unexpected", one per
     "expected"). */
  int yycount = 0;

  /* There are many possibilities here to consider:
     - If this state is a consistent state with a default action, then
       the only way this function was invoked is if the default action
       is an error action.  In that case, don't check for expected
       tokens because there are none.
     - The only way there can be no lookahead present (in yychar) is if
       this state is a consistent state with a default action.  Thus,
       detecting the absence of a lookahead is sufficient to determine
       that there is no unexpected or expected token to report.  In that
       case, just report a simple "syntax error".
     - Don't assume there isn't a lookahead just because this state is a
       consistent state with a default action.  There might have been a
       previous inconsistent state, consistent state with a non-default
       action, or user semantic action that manipulated yychar.
     - Of course, the expected token list depends on states to have
       correct lookahead information, and it depends on the parser not
       to perform extra reductions after fetching a lookahead from the
       scanner and before detecting a syntax error.  Thus, state merging
       (from LALR or IELR) and default reductions corrupt the expected
       token list.  However, the list is correct for canonical LR with
       one exception: it will still contain any token that will not be
       accepted due to an error action in a later state.
  */
  if (yytoken != YYEMPTY)
    {
      int yyn = yypact[*yyssp];
      yyarg[yycount++] = yytname[yytoken];
      if (!yypact_value_is_default (yyn))
        {
          /* Start YYX at -YYN if negative to avoid negative indexes in
             YYCHECK.  In other words, skip the first -YYN actions for
             this state because they are default actions.  */
          int yyxbegin = yyn < 0 ? -yyn : 0;
          /* Stay within bounds of both yycheck and yytname.  */
          int yychecklim = YYLAST - yyn + 1;
          int yyxend = yychecklim < YYNTOKENS ? yychecklim : YYNTOKENS;
          int yyx;

          for (yyx = yyxbegin; yyx < yyxend; ++yyx)
            if (yycheck[yyx + yyn] == yyx && yyx != YYTERROR
                && !yytable_value_is_error (yytable[yyx + yyn]))
              {
                if (yycount == YYERROR_VERBOSE_ARGS_MAXIMUM)
                  {
                    yycount = 1;
                    yysize = yysize0;
                    break;
                  }
                yyarg[yycount++] = yytname[yyx];
                {
                  YYSIZE_T yysize1 = yysize + yytnamerr (YY_NULLPTR, yytname[yyx]);
                  if (! (yysize <= yysize1
                         && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
                    return 2;
                  yysize = yysize1;
                }
              }
        }
    }

  switch (yycount)
    {
# define YYCASE_(N, S)                      \
      case N:                               \
        yyformat = S;                       \
      break
      YYCASE_(0, YY_("syntax error"));
      YYCASE_(1, YY_("syntax error, unexpected %s"));
      YYCASE_(2, YY_("syntax error, unexpected %s, expecting %s"));
      YYCASE_(3, YY_("syntax error, unexpected %s, expecting %s or %s"));
      YYCASE_(4, YY_("syntax error, unexpected %s, expecting %s or %s or %s"));
      YYCASE_(5, YY_("syntax error, unexpected %s, expecting %s or %s or %s or %s"));
# undef YYCASE_
    }

  {
    YYSIZE_T yysize1 = yysize + yystrlen (yyformat);
    if (! (yysize <= yysize1 && yysize1 <= YYSTACK_ALLOC_MAXIMUM))
      return 2;
    yysize = yysize1;
  }

  if (*yymsg_alloc < yysize)
    {
      *yymsg_alloc = 2 * yysize;
      if (! (yysize <= *yymsg_alloc
             && *yymsg_alloc <= YYSTACK_ALLOC_MAXIMUM))
        *yymsg_alloc = YYSTACK_ALLOC_MAXIMUM;
      return 1;
    }

  /* Avoid sprintf, as that infringes on the user's name space.
     Don't have undefined behavior even if the translation
     produced a string with the wrong number of "%s"s.  */
  {
    char *yyp = *yymsg;
    int yyi = 0;
    while ((*yyp = *yyformat) != '\0')
      if (*yyp == '%' && yyformat[1] == 's' && yyi < yycount)
        {
          yyp += yytnamerr (yyp, yyarg[yyi++]);
          yyformat += 2;
        }
      else
        {
          yyp++;
          yyformat++;
        }
  }
  return 0;
}
#endif /* YYERROR_VERBOSE */

/*-----------------------------------------------.
| Release the memory associated to this symbol.  |
`-----------------------------------------------*/

static void
yydestruct (const char *yymsg, int yytype, YYSTYPE *yyvaluep)
{
  YYUSE (yyvaluep);
  if (!yymsg)
    yymsg = "Deleting";
  YY_SYMBOL_PRINT (yymsg, yytype, yyvaluep, yylocationp);

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  YYUSE (yytype);
  YY_IGNORE_MAYBE_UNINITIALIZED_END
}




/* The lookahead symbol.  */
int yychar;

/* The semantic value of the lookahead symbol.  */
YYSTYPE yylval;
/* Number of syntax errors so far.  */
int yynerrs;


/*----------.
| yyparse.  |
`----------*/

int
yyparse (void)
{
    int yystate;
    /* Number of tokens to shift before error messages enabled.  */
    int yyerrstatus;

    /* The stacks and their tools:
       'yyss': related to states.
       'yyvs': related to semantic values.

       Refer to the stacks through separate pointers, to allow yyoverflow
       to reallocate them elsewhere.  */

    /* The state stack.  */
    yytype_int16 yyssa[YYINITDEPTH];
    yytype_int16 *yyss;
    yytype_int16 *yyssp;

    /* The semantic value stack.  */
    YYSTYPE yyvsa[YYINITDEPTH];
    YYSTYPE *yyvs;
    YYSTYPE *yyvsp;

    YYSIZE_T yystacksize;

  int yyn;
  int yyresult;
  /* Lookahead token as an internal (translated) token number.  */
  int yytoken = 0;
  /* The variables used to return semantic value and location from the
     action routines.  */
  YYSTYPE yyval;

#if YYERROR_VERBOSE
  /* Buffer for error messages, and its allocated size.  */
  char yymsgbuf[128];
  char *yymsg = yymsgbuf;
  YYSIZE_T yymsg_alloc = sizeof yymsgbuf;
#endif

#define YYPOPSTACK(N)   (yyvsp -= (N), yyssp -= (N))

  /* The number of symbols on the RHS of the reduced rule.
     Keep to zero when no symbol should be popped.  */
  int yylen = 0;

  yyssp = yyss = yyssa;
  yyvsp = yyvs = yyvsa;
  yystacksize = YYINITDEPTH;

  YYDPRINTF ((stderr, "Starting parse\n"));

  yystate = 0;
  yyerrstatus = 0;
  yynerrs = 0;
  yychar = YYEMPTY; /* Cause a token to be read.  */
  goto yysetstate;

/*------------------------------------------------------------.
| yynewstate -- Push a new state, which is found in yystate.  |
`------------------------------------------------------------*/
 yynewstate:
  /* In all cases, when you get here, the value and location stacks
     have just been pushed.  So pushing a state here evens the stacks.  */
  yyssp++;

 yysetstate:
  *yyssp = yystate;

  if (yyss + yystacksize - 1 <= yyssp)
    {
      /* Get the current used size of the three stacks, in elements.  */
      YYSIZE_T yysize = yyssp - yyss + 1;

#ifdef yyoverflow
      {
        /* Give user a chance to reallocate the stack.  Use copies of
           these so that the &'s don't force the real ones into
           memory.  */
        YYSTYPE *yyvs1 = yyvs;
        yytype_int16 *yyss1 = yyss;

        /* Each stack pointer address is followed by the size of the
           data in use in that stack, in bytes.  This used to be a
           conditional around just the two extra args, but that might
           be undefined if yyoverflow is a macro.  */
        yyoverflow (YY_("memory exhausted"),
                    &yyss1, yysize * sizeof (*yyssp),
                    &yyvs1, yysize * sizeof (*yyvsp),
                    &yystacksize);

        yyss = yyss1;
        yyvs = yyvs1;
      }
#else /* no yyoverflow */
# ifndef YYSTACK_RELOCATE
      goto yyexhaustedlab;
# else
      /* Extend the stack our own way.  */
      if (YYMAXDEPTH <= yystacksize)
        goto yyexhaustedlab;
      yystacksize *= 2;
      if (YYMAXDEPTH < yystacksize)
        yystacksize = YYMAXDEPTH;

      {
        yytype_int16 *yyss1 = yyss;
        union yyalloc *yyptr =
          (union yyalloc *) YYSTACK_ALLOC (YYSTACK_BYTES (yystacksize));
        if (! yyptr)
          goto yyexhaustedlab;
        YYSTACK_RELOCATE (yyss_alloc, yyss);
        YYSTACK_RELOCATE (yyvs_alloc, yyvs);
#  undef YYSTACK_RELOCATE
        if (yyss1 != yyssa)
          YYSTACK_FREE (yyss1);
      }
# endif
#endif /* no yyoverflow */

      yyssp = yyss + yysize - 1;
      yyvsp = yyvs + yysize - 1;

      YYDPRINTF ((stderr, "Stack size increased to %lu\n",
                  (unsigned long int) yystacksize));

      if (yyss + yystacksize - 1 <= yyssp)
        YYABORT;
    }

  YYDPRINTF ((stderr, "Entering state %d\n", yystate));

  if (yystate == YYFINAL)
    YYACCEPT;

  goto yybackup;

/*-----------.
| yybackup.  |
`-----------*/
yybackup:

  /* Do appropriate processing given the current state.  Read a
     lookahead token if we need one and don't already have one.  */

  /* First try to decide what to do without reference to lookahead token.  */
  yyn = yypact[yystate];
  if (yypact_value_is_default (yyn))
    goto yydefault;

  /* Not known => get a lookahead token if don't already have one.  */

  /* YYCHAR is either YYEMPTY or YYEOF or a valid lookahead symbol.  */
  if (yychar == YYEMPTY)
    {
      YYDPRINTF ((stderr, "Reading a token: "));
      yychar = yylex ();
    }

  if (yychar <= YYEOF)
    {
      yychar = yytoken = YYEOF;
      YYDPRINTF ((stderr, "Now at end of input.\n"));
    }
  else
    {
      yytoken = YYTRANSLATE (yychar);
      YY_SYMBOL_PRINT ("Next token is", yytoken, &yylval, &yylloc);
    }

  /* If the proper action on seeing token YYTOKEN is to reduce or to
     detect an error, take that action.  */
  yyn += yytoken;
  if (yyn < 0 || YYLAST < yyn || yycheck[yyn] != yytoken)
    goto yydefault;
  yyn = yytable[yyn];
  if (yyn <= 0)
    {
      if (yytable_value_is_error (yyn))
        goto yyerrlab;
      yyn = -yyn;
      goto yyreduce;
    }

  /* Count tokens shifted since error; after three, turn off error
     status.  */
  if (yyerrstatus)
    yyerrstatus--;

  /* Shift the lookahead token.  */
  YY_SYMBOL_PRINT ("Shifting", yytoken, &yylval, &yylloc);

  /* Discard the shifted token.  */
  yychar = YYEMPTY;

  yystate = yyn;
  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END

  goto yynewstate;


/*-----------------------------------------------------------.
| yydefault -- do the default action for the current state.  |
`-----------------------------------------------------------*/
yydefault:
  yyn = yydefact[yystate];
  if (yyn == 0)
    goto yyerrlab;
  goto yyreduce;


/*-----------------------------.
| yyreduce -- Do a reduction.  |
`-----------------------------*/
yyreduce:
  /* yyn is the number of a rule to reduce with.  */
  yylen = yyr2[yyn];

  /* If YYLEN is nonzero, implement the default value of the action:
     '$$ = $1'.

     Otherwise, the following line sets YYVAL to garbage.
     This behavior is undocumented and Bison
     users should not rely upon it.  Assigning to YYVAL
     unconditionally makes the parser a bit smaller, and it avoids a
     GCC warning that YYVAL may be used uninitialized.  */
  yyval = yyvsp[1-yylen];


  YY_REDUCE_PRINT (yyn);
  switch (yyn)
    {
        case 13:
#line 171 "parse_tab.y" /* yacc.c:1646  */
    { 
    _warnmsg("'tcpwrappers' without yes|no"); 
    conf_set_use_tcp_wrappers(TRUE);
}
#line 1528 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 14:
#line 174 "parse_tab.y" /* yacc.c:1646  */
    { 
    conf_set_use_tcp_wrappers(TRUE);
}
#line 1536 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 15:
#line 176 "parse_tab.y" /* yacc.c:1646  */
    { 
    conf_set_use_tcp_wrappers(FALSE);
}
#line 1544 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 16:
#line 180 "parse_tab.y" /* yacc.c:1646  */
    { 
    conf_set_plug_log_level((yyvsp[0]));
}
#line 1552 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 17:
#line 184 "parse_tab.y" /* yacc.c:1646  */
    { 
    conf_add_listen((yyvsp[0]));
}
#line 1560 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 18:
#line 189 "parse_tab.y" /* yacc.c:1646  */
    {
    makeDevice((yyvsp[-3]), (yyvsp[-2]), (yyvsp[-1]), (yyvsp[0]));
}
#line 1568 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 19:
#line 191 "parse_tab.y" /* yacc.c:1646  */
    {
    makeDevice((yyvsp[-2]), (yyvsp[-1]), (yyvsp[0]), NULL);
}
#line 1576 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 20:
#line 195 "parse_tab.y" /* yacc.c:1646  */
    {
    makeNode((yyvsp[-2]), (yyvsp[-1]), (yyvsp[0]));
}
#line 1584 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 21:
#line 197 "parse_tab.y" /* yacc.c:1646  */
    {
    makeNode((yyvsp[-1]), (yyvsp[0]), NULL);
}
#line 1592 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 22:
#line 201 "parse_tab.y" /* yacc.c:1646  */
    {
    makeAlias((yyvsp[-1]), (yyvsp[0]));
}
#line 1600 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 23:
#line 210 "parse_tab.y" /* yacc.c:1646  */
    {
    makeSpec((yyvsp[-3]));
}
#line 1608 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 30:
#line 222 "parse_tab.y" /* yacc.c:1646  */
    {
    _doubletotv(&current_spec.timeout, _strtodouble((yyvsp[0])));
}
#line 1616 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 31:
#line 226 "parse_tab.y" /* yacc.c:1646  */
    {
    _doubletotv(&current_spec.ping_period, _strtodouble((yyvsp[0])));
}
#line 1624 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 32:
#line 230 "parse_tab.y" /* yacc.c:1646  */
    {
    list_append((List)(yyvsp[-1]), xstrdup((yyvsp[0]))); 
    (yyval) = (yyvsp[-1]); 
}
#line 1633 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 33:
#line 233 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)list_create((ListDelF)xfree);
    list_append((List)(yyval), xstrdup((yyvsp[0]))); 
}
#line 1642 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 34:
#line 238 "parse_tab.y" /* yacc.c:1646  */
    {
    if (current_spec.plugs != NULL)
        _errormsg("duplicate plug list");
    current_spec.plugs = (List)(yyvsp[-1]);
}
#line 1652 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 37:
#line 247 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_LOG_IN, (List)(yyvsp[0]));
}
#line 1660 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 38:
#line 249 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_LOG_OUT, (List)(yyvsp[0]));
}
#line 1668 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 39:
#line 251 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_STATUS_PLUGS, (List)(yyvsp[0]));
}
#line 1676 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 40:
#line 253 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_STATUS_PLUGS_ALL, (List)(yyvsp[0]));
}
#line 1684 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 41:
#line 255 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_STATUS_TEMP, (List)(yyvsp[0]));
}
#line 1692 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 42:
#line 257 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_STATUS_TEMP_ALL, (List)(yyvsp[0]));
}
#line 1700 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 43:
#line 259 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_STATUS_BEACON, (List)(yyvsp[0]));
}
#line 1708 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 44:
#line 261 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_STATUS_BEACON_ALL, (List)(yyvsp[0]));
}
#line 1716 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 45:
#line 263 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_BEACON_ON, (List)(yyvsp[0]));
}
#line 1724 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 46:
#line 265 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_BEACON_ON_RANGED, (List)(yyvsp[0]));
}
#line 1732 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 47:
#line 267 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_BEACON_OFF, (List)(yyvsp[0]));
}
#line 1740 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 48:
#line 269 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_BEACON_OFF_RANGED, (List)(yyvsp[0]));
}
#line 1748 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 49:
#line 271 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_POWER_ON, (List)(yyvsp[0]));
}
#line 1756 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 50:
#line 273 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_POWER_ON_RANGED, (List)(yyvsp[0]));
}
#line 1764 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 51:
#line 275 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_POWER_ON_ALL, (List)(yyvsp[0]));
}
#line 1772 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 52:
#line 277 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_POWER_OFF, (List)(yyvsp[0]));
}
#line 1780 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 53:
#line 279 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_POWER_OFF_RANGED, (List)(yyvsp[0]));
}
#line 1788 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 54:
#line 281 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_POWER_OFF_ALL, (List)(yyvsp[0]));
}
#line 1796 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 55:
#line 283 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_POWER_CYCLE, (List)(yyvsp[0]));
}
#line 1804 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 56:
#line 285 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_POWER_CYCLE_RANGED, (List)(yyvsp[0]));
}
#line 1812 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 57:
#line 287 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_POWER_CYCLE_ALL, (List)(yyvsp[0]));
}
#line 1820 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 58:
#line 289 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_RESET, (List)(yyvsp[0]));
}
#line 1828 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 59:
#line 291 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_RESET_RANGED, (List)(yyvsp[0]));
}
#line 1836 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 60:
#line 293 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_RESET_ALL, (List)(yyvsp[0]));
}
#line 1844 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 61:
#line 295 "parse_tab.y" /* yacc.c:1646  */
    {
    makeScript(PM_PING, (List)(yyvsp[0]));
}
#line 1852 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 62:
#line 299 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (yyvsp[-1]);
}
#line 1860 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 63:
#line 303 "parse_tab.y" /* yacc.c:1646  */
    { 
    list_append((List)(yyvsp[-1]), (yyvsp[0])); 
    (yyval) = (yyvsp[-1]); 
}
#line 1869 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 64:
#line 306 "parse_tab.y" /* yacc.c:1646  */
    { 
    (yyval) = (char *)list_create((ListDelF)destroyPreStmt);
    list_append((List)(yyval), (yyvsp[0])); 
}
#line 1878 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 65:
#line 311 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makePreStmt(STMT_EXPECT, (yyvsp[0]), NULL, NULL, NULL, NULL, NULL);
}
#line 1886 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 66:
#line 313 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makePreStmt(STMT_SEND, (yyvsp[0]), NULL, NULL, NULL, NULL, NULL);
}
#line 1894 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 67:
#line 315 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makePreStmt(STMT_DELAY, NULL, (yyvsp[0]), NULL, NULL, NULL, NULL);
}
#line 1902 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 68:
#line 317 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makePreStmt(STMT_SETPLUGSTATE, (yyvsp[-1]), NULL, NULL, (yyvsp[0]), NULL, NULL);
}
#line 1910 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 69:
#line 319 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makePreStmt(STMT_SETPLUGSTATE, (yyvsp[-2]), NULL, NULL, (yyvsp[-1]), NULL,
                             (List)(yyvsp[0]));
}
#line 1919 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 70:
#line 322 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makePreStmt(STMT_SETPLUGSTATE, NULL, NULL, (yyvsp[-1]), (yyvsp[0]), NULL, NULL);
}
#line 1927 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 71:
#line 324 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makePreStmt(STMT_SETPLUGSTATE, NULL, NULL, (yyvsp[-2]), (yyvsp[-1]), NULL,(List)(yyvsp[0]));
}
#line 1935 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 72:
#line 326 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makePreStmt(STMT_SETPLUGSTATE, NULL, NULL, NULL, (yyvsp[0]), NULL, NULL);
}
#line 1943 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 73:
#line 328 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makePreStmt(STMT_SETPLUGSTATE, NULL, NULL, NULL,(yyvsp[-1]),NULL,(List)(yyvsp[0]));
}
#line 1951 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 74:
#line 330 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makePreStmt(STMT_FOREACHNODE, NULL, NULL, NULL, NULL, 
                             (List)(yyvsp[0]), NULL);
}
#line 1960 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 75:
#line 333 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makePreStmt(STMT_FOREACHPLUG, NULL, NULL, NULL, NULL, 
                             (List)(yyvsp[0]), NULL);
}
#line 1969 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 76:
#line 336 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makePreStmt(STMT_IFOFF, NULL, NULL, NULL, NULL, 
                             (List)(yyvsp[0]), NULL);
}
#line 1978 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 77:
#line 339 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makePreStmt(STMT_IFON, NULL, NULL, NULL, NULL, 
                             (List)(yyvsp[0]), NULL);
}
#line 1987 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 78:
#line 344 "parse_tab.y" /* yacc.c:1646  */
    { 
    list_append((List)(yyvsp[-1]), (yyvsp[0])); 
    (yyval) = (yyvsp[-1]); 
}
#line 1996 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 79:
#line 347 "parse_tab.y" /* yacc.c:1646  */
    { 
    (yyval) = (char *)list_create((ListDelF)destroyInterp);
    list_append((List)(yyval), (yyvsp[0])); 
}
#line 2005 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 80:
#line 352 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makeInterp(ST_ON, (yyvsp[0]));
}
#line 2013 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 81:
#line 354 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (char *)makeInterp(ST_OFF, (yyvsp[0]));
}
#line 2021 "parse_tab.c" /* yacc.c:1646  */
    break;

  case 82:
#line 358 "parse_tab.y" /* yacc.c:1646  */
    {
    (yyval) = (yyvsp[0]);
}
#line 2029 "parse_tab.c" /* yacc.c:1646  */
    break;


#line 2033 "parse_tab.c" /* yacc.c:1646  */
      default: break;
    }
  /* User semantic actions sometimes alter yychar, and that requires
     that yytoken be updated with the new translation.  We take the
     approach of translating immediately before every use of yytoken.
     One alternative is translating here after every semantic action,
     but that translation would be missed if the semantic action invokes
     YYABORT, YYACCEPT, or YYERROR immediately after altering yychar or
     if it invokes YYBACKUP.  In the case of YYABORT or YYACCEPT, an
     incorrect destructor might then be invoked immediately.  In the
     case of YYERROR or YYBACKUP, subsequent parser actions might lead
     to an incorrect destructor call or verbose syntax error message
     before the lookahead is translated.  */
  YY_SYMBOL_PRINT ("-> $$ =", yyr1[yyn], &yyval, &yyloc);

  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);

  *++yyvsp = yyval;

  /* Now 'shift' the result of the reduction.  Determine what state
     that goes to, based on the state we popped back to and the rule
     number reduced by.  */

  yyn = yyr1[yyn];

  yystate = yypgoto[yyn - YYNTOKENS] + *yyssp;
  if (0 <= yystate && yystate <= YYLAST && yycheck[yystate] == *yyssp)
    yystate = yytable[yystate];
  else
    yystate = yydefgoto[yyn - YYNTOKENS];

  goto yynewstate;


/*--------------------------------------.
| yyerrlab -- here on detecting error.  |
`--------------------------------------*/
yyerrlab:
  /* Make sure we have latest lookahead translation.  See comments at
     user semantic actions for why this is necessary.  */
  yytoken = yychar == YYEMPTY ? YYEMPTY : YYTRANSLATE (yychar);

  /* If not already recovering from an error, report this error.  */
  if (!yyerrstatus)
    {
      ++yynerrs;
#if ! YYERROR_VERBOSE
      yyerror (YY_("syntax error"));
#else
# define YYSYNTAX_ERROR yysyntax_error (&yymsg_alloc, &yymsg, \
                                        yyssp, yytoken)
      {
        char const *yymsgp = YY_("syntax error");
        int yysyntax_error_status;
        yysyntax_error_status = YYSYNTAX_ERROR;
        if (yysyntax_error_status == 0)
          yymsgp = yymsg;
        else if (yysyntax_error_status == 1)
          {
            if (yymsg != yymsgbuf)
              YYSTACK_FREE (yymsg);
            yymsg = (char *) YYSTACK_ALLOC (yymsg_alloc);
            if (!yymsg)
              {
                yymsg = yymsgbuf;
                yymsg_alloc = sizeof yymsgbuf;
                yysyntax_error_status = 2;
              }
            else
              {
                yysyntax_error_status = YYSYNTAX_ERROR;
                yymsgp = yymsg;
              }
          }
        yyerror (yymsgp);
        if (yysyntax_error_status == 2)
          goto yyexhaustedlab;
      }
# undef YYSYNTAX_ERROR
#endif
    }



  if (yyerrstatus == 3)
    {
      /* If just tried and failed to reuse lookahead token after an
         error, discard it.  */

      if (yychar <= YYEOF)
        {
          /* Return failure if at end of input.  */
          if (yychar == YYEOF)
            YYABORT;
        }
      else
        {
          yydestruct ("Error: discarding",
                      yytoken, &yylval);
          yychar = YYEMPTY;
        }
    }

  /* Else will try to reuse lookahead token after shifting the error
     token.  */
  goto yyerrlab1;


/*---------------------------------------------------.
| yyerrorlab -- error raised explicitly by YYERROR.  |
`---------------------------------------------------*/
yyerrorlab:

  /* Pacify compilers like GCC when the user code never invokes
     YYERROR and the label yyerrorlab therefore never appears in user
     code.  */
  if (/*CONSTCOND*/ 0)
     goto yyerrorlab;

  /* Do not reclaim the symbols of the rule whose action triggered
     this YYERROR.  */
  YYPOPSTACK (yylen);
  yylen = 0;
  YY_STACK_PRINT (yyss, yyssp);
  yystate = *yyssp;
  goto yyerrlab1;


/*-------------------------------------------------------------.
| yyerrlab1 -- common code for both syntax error and YYERROR.  |
`-------------------------------------------------------------*/
yyerrlab1:
  yyerrstatus = 3;      /* Each real token shifted decrements this.  */

  for (;;)
    {
      yyn = yypact[yystate];
      if (!yypact_value_is_default (yyn))
        {
          yyn += YYTERROR;
          if (0 <= yyn && yyn <= YYLAST && yycheck[yyn] == YYTERROR)
            {
              yyn = yytable[yyn];
              if (0 < yyn)
                break;
            }
        }

      /* Pop the current state because it cannot handle the error token.  */
      if (yyssp == yyss)
        YYABORT;


      yydestruct ("Error: popping",
                  yystos[yystate], yyvsp);
      YYPOPSTACK (1);
      yystate = *yyssp;
      YY_STACK_PRINT (yyss, yyssp);
    }

  YY_IGNORE_MAYBE_UNINITIALIZED_BEGIN
  *++yyvsp = yylval;
  YY_IGNORE_MAYBE_UNINITIALIZED_END


  /* Shift the error token.  */
  YY_SYMBOL_PRINT ("Shifting", yystos[yyn], yyvsp, yylsp);

  yystate = yyn;
  goto yynewstate;


/*-------------------------------------.
| yyacceptlab -- YYACCEPT comes here.  |
`-------------------------------------*/
yyacceptlab:
  yyresult = 0;
  goto yyreturn;

/*-----------------------------------.
| yyabortlab -- YYABORT comes here.  |
`-----------------------------------*/
yyabortlab:
  yyresult = 1;
  goto yyreturn;

#if !defined yyoverflow || YYERROR_VERBOSE
/*-------------------------------------------------.
| yyexhaustedlab -- memory exhaustion comes here.  |
`-------------------------------------------------*/
yyexhaustedlab:
  yyerror (YY_("memory exhausted"));
  yyresult = 2;
  /* Fall through.  */
#endif

yyreturn:
  if (yychar != YYEMPTY)
    {
      /* Make sure we have latest lookahead translation.  See comments at
         user semantic actions for why this is necessary.  */
      yytoken = YYTRANSLATE (yychar);
      yydestruct ("Cleanup: discarding lookahead",
                  yytoken, &yylval);
    }
  /* Do not reclaim the symbols of the rule whose action triggered
     this YYABORT or YYACCEPT.  */
  YYPOPSTACK (yylen);
  YY_STACK_PRINT (yyss, yyssp);
  while (yyssp != yyss)
    {
      yydestruct ("Cleanup: popping",
                  yystos[*yyssp], yyvsp);
      YYPOPSTACK (1);
    }
#ifndef yyoverflow
  if (yyss != yyssa)
    YYSTACK_FREE (yyss);
#endif
#if YYERROR_VERBOSE
  if (yymsg != yymsgbuf)
    YYSTACK_FREE (yymsg);
#endif
  return yyresult;
}
#line 362 "parse_tab.y" /* yacc.c:1906  */


void scanner_init(char *filename);
void scanner_fini(void);
int scanner_line(void);
char *scanner_file(void);

/*
 * Entry point into the yacc/lex parser.
 */
int parse_config_file (char *filename)
{
    extern FILE *yyin; /* part of lexer */

    scanner_init(filename);

    yyin = fopen(filename, "r");
    if (!yyin)
        err_exit(TRUE, "%s", filename);

    device_specs = list_create((ListDelF) destroySpec);
    _clear_current_spec();

    yyparse();
    fclose(yyin);

    scanner_fini();

    list_destroy(device_specs);

    return 0;
} 

/* makePreStmt(type, str, tv, mp1(plug), mp2(stat/node), prestmts, interps */
static PreStmt *makePreStmt(StmtType type, char *str, char *tvstr, 
                      char *mp1str, char *mp2str, List prestmts, 
                      List interps)
{
    PreStmt *new;

    new = (PreStmt *) xmalloc(sizeof(PreStmt));

    new->magic = PRESTMT_MAGIC;
    new->type = type;
    new->mp1 = mp1str ? _strtolong(mp1str) : -1;
    new->mp2 = mp2str ? _strtolong(mp2str) : -1;
    if (str)
        new->str = xstrdup(str);
    if (tvstr)
        _doubletotv(&new->tv, _strtodouble(tvstr));
    new->prestmts = prestmts;
    new->interps = interps;

    return new;
}

static void destroyPreStmt(PreStmt *p)
{
    assert(p->magic == PRESTMT_MAGIC);
    p->magic = 0;
    if (p->str)
        xfree(p->str);
    p->str = NULL;
    if (p->prestmts)
        list_destroy(p->prestmts);
    p->prestmts = NULL;
    if (p->interps)
        list_destroy(p->interps);
    p->interps = NULL;
    xfree(p);
}

static void _clear_current_spec(void)
{
    int i;

    current_spec.name = NULL;
    current_spec.plugs = NULL;
    timerclear(&current_spec.timeout);
    timerclear(&current_spec.ping_period);
    for (i = 0; i < NUM_SCRIPTS; i++)
        current_spec.prescripts[i] = NULL;
}

static Spec *_copy_current_spec(void)
{
    Spec *new = (Spec *) xmalloc(sizeof(Spec));
    int i;

    *new = current_spec;
    for (i = 0; i < NUM_SCRIPTS; i++)
        new->prescripts[i] = current_spec.prescripts[i];
    _clear_current_spec();
    return new;
}

static Spec *makeSpec(char *name)
{
    Spec *spec;

    current_spec.name = xstrdup(name);

    /* FIXME: check for manditory scripts here?  what are they? */

    spec = _copy_current_spec();
    assert(device_specs != NULL);
    list_append(device_specs, spec);

    return spec;
}

static void destroySpec(Spec * spec)
{
    int i;

    if (spec->name)
        xfree(spec->name);
    if (spec->plugs)
        list_destroy(spec->plugs);
    for (i = 0; i < NUM_SCRIPTS; i++)
        if (spec->prescripts[i])
            list_destroy(spec->prescripts[i]);
    xfree(spec);
}

static int matchSpec(Spec * spec, void *key)
{
    return (strcmp(spec->name, (char *) key) == 0);
}

static Spec *findSpec(char *name)
{
    return list_find_first(device_specs, (ListFindF) matchSpec, name);
}

static void makeScript(int com, List stmts)
{
    if (current_spec.prescripts[com] != NULL)
        _errormsg("duplicate script");
    current_spec.prescripts[com] = stmts;
}

static Interp *makeInterp(InterpState state, char *str)
{
    Interp *new = (Interp *)xmalloc(sizeof(Interp));

    new->magic = INTERP_MAGIC; 
    new->str = xstrdup(str); 
    new->re = xregex_create();
    new->state = state;

    return new;
}

static void destroyInterp(Interp *i)
{
    assert(i->magic == INTERP_MAGIC);
    i->magic = 0;
    xfree(i->str);
    xregex_destroy(i->re);
    xfree(i);
}

static List copyInterpList(List il)
{
    ListIterator itr;
    Interp *ip, *icpy;
    List new = list_create((ListDelF) destroyInterp);

    if (il != NULL) {

        itr = list_iterator_create(il);
        while((ip = list_next(itr))) {
            assert(ip->magic == INTERP_MAGIC);
            icpy = makeInterp(ip->state, ip->str);
            xregex_compile(icpy->re, icpy->str, FALSE);
            assert(icpy->magic == INTERP_MAGIC);
            list_append(new, icpy);
        }
        list_iterator_destroy(itr);
    }

    return new;
}

/**
 ** Powerman.conf stuff.
 **/

static void destroyStmt(Stmt *stmt)
{
    assert(stmt != NULL);

    switch (stmt->type) {
    case STMT_SEND:
        xfree(stmt->u.send.fmt);
        break;
    case STMT_EXPECT:
        xregex_destroy(stmt->u.expect.exp);
        break;
    case STMT_DELAY:
        break;
    case STMT_SETPLUGSTATE:
        list_destroy(stmt->u.setplugstate.interps);
        break;
    case STMT_FOREACHNODE:
    case STMT_FOREACHPLUG:
    case STMT_IFON:
    case STMT_IFOFF:
        list_destroy(stmt->u.foreach.stmts);
        break;
    default:
        break;
    }
    xfree(stmt);
}

static Stmt *makeStmt(PreStmt *p)
{
    Stmt *stmt;
    PreStmt *subp;
    ListIterator itr;

    assert(p->magic == PRESTMT_MAGIC);
    stmt = (Stmt *) xmalloc(sizeof(Stmt));
    stmt->type = p->type;
    switch (p->type) {
    case STMT_SEND:
        stmt->u.send.fmt = xstrdup(p->str);
        break;
    case STMT_EXPECT:
        stmt->u.expect.exp = xregex_create(); 
        xregex_compile(stmt->u.expect.exp, p->str, TRUE);
        break;
    case STMT_SETPLUGSTATE:
        stmt->u.setplugstate.stat_mp = p->mp2;
        if (p->str)
            stmt->u.setplugstate.plug_name = xstrdup(p->str);
        else
            stmt->u.setplugstate.plug_mp = p->mp1;
        stmt->u.setplugstate.interps = copyInterpList(p->interps);
        break;
    case STMT_DELAY:
        stmt->u.delay.tv = p->tv;
        break;
    case STMT_FOREACHNODE:
    case STMT_FOREACHPLUG:
        stmt->u.foreach.stmts = list_create((ListDelF) destroyStmt);
        itr = list_iterator_create(p->prestmts);
        while((subp = list_next(itr))) {
            assert(subp->magic == PRESTMT_MAGIC);
            list_append(stmt->u.foreach.stmts, makeStmt(subp));
        }
        list_iterator_destroy(itr);
        break;
    case STMT_IFON:
    case STMT_IFOFF:
        stmt->u.ifonoff.stmts = list_create((ListDelF) destroyStmt);
        itr = list_iterator_create(p->prestmts);
        while((subp = list_next(itr))) {
            assert(subp->magic == PRESTMT_MAGIC);
            list_append(stmt->u.ifonoff.stmts, makeStmt(subp));
        }
        list_iterator_destroy(itr);
        break;
    default:
        break;
    }
    return stmt;
}

static void _parse_hoststr(Device *dev, char *hoststr, char *flagstr)
{
    /* pipe device, e.g. "conman -j baytech0 |&" */
    if (strstr(hoststr, "|&") != NULL) {
        dev->data           = pipe_create(hoststr, flagstr);
        dev->destroy        = pipe_destroy;
        dev->connect        = pipe_connect;
        dev->disconnect     = pipe_disconnect;
        dev->finish_connect = NULL;
        dev->preprocess     = NULL;

    /* serial device, e.g. "/dev/ttyS0" */
    } else if (hoststr[0] == '/') {
        struct stat sb;

        if (stat(hoststr, &sb) == -1 || (!(sb.st_mode & S_IFCHR))) 
            _errormsg("serial device not found or not a char special file");

        dev->data           = serial_create(hoststr, flagstr);
        dev->destroy        = serial_destroy;
        dev->connect        = serial_connect;
        dev->disconnect     = serial_disconnect;
        dev->finish_connect = NULL;
        dev->preprocess     = NULL;

    /* tcp device, e.g. "cyclades0:2001" */
    } else {
        char *port = strchr(hoststr, ':');
        int n;

        if (port) {                 /* host='host:port', flags=NULL */
            *port++ = '\0';
        } else
            _errormsg("hostname is missing :port");

        n = _strtolong(port);       /* verify port number */
        if (n < 1 || n > 65535)
            _errormsg("port number out of range");
        dev->data           = tcp_create(hoststr, port, flagstr);
        dev->destroy        = tcp_destroy;
        dev->connect        = tcp_connect;
        dev->disconnect     = tcp_disconnect;
        dev->finish_connect = tcp_finish_connect;
        dev->preprocess     = tcp_preprocess;
    }
}

static void makeDevice(char *devstr, char *specstr, char *hoststr, 
                        char *flagstr)
{
    ListIterator itr;
    Device *dev;
    Spec *spec;
    int i;

    /* find that spec */
    spec = findSpec(specstr);
    if ( spec == NULL ) 
        _errormsg("device specification not found");

    /* make the Device */
    dev = dev_create(devstr);
    dev->specname = xstrdup(specstr);
    dev->timeout = spec->timeout;
    dev->ping_period = spec->ping_period;

    _parse_hoststr(dev, hoststr, flagstr);

    /* create plugs (spec->plugs may be NULL) */
    dev->plugs = pluglist_create(spec->plugs);

    /* transfer remaining info from the spec to the device */
    for (i = 0; i < NUM_SCRIPTS; i++) {
        PreStmt *p;

        if (spec->prescripts[i] == NULL) {
            dev->scripts[i] = NULL;
            continue; /* unimplemented script */
        }

        dev->scripts[i] = list_create((ListDelF) destroyStmt);

        /* copy the list of statements in each script */
        itr = list_iterator_create(spec->prescripts[i]);
        while((p = list_next(itr))) {
            list_append(dev->scripts[i], makeStmt(p));
        }
        list_iterator_destroy(itr);
    }

    dev_add(dev);
}

static void makeAlias(char *namestr, char *hostsstr)
{
    if (!conf_add_alias(namestr, hostsstr))
        _errormsg("bad alias");
}

static void makeNode(char *nodestr, char *devstr, char *plugstr)
{
    Device *dev = dev_findbyname(devstr);

    if (dev == NULL) 
        _errormsg("unknown device");

    /* plugstr can be NULL - see comment in pluglist.h */
    switch (pluglist_map(dev->plugs, nodestr, plugstr)) {
        case EPL_DUPNODE:
            _errormsg("duplicate node");
        case EPL_UNKPLUG:
            _errormsg("unknown plug name");
        case EPL_DUPPLUG:
            _errormsg("plug already assigned");
        case EPL_NOPLUGS:
            _errormsg("more nodes than plugs");
        case EPL_NONODES:
            _errormsg("more plugs than nodes");
        default:
            break;
    }

    if (!conf_addnodes(nodestr))
        _errormsg("duplicate node name");
}

/**
 ** Utility functions
 **/

static double _strtodouble(char *str)
{
    char *endptr;
    double val = strtod(str, &endptr);

    if (val == 0.0 && endptr == str)
        _errormsg("error parsing double value");
    if ((val == HUGE_VAL || val == -HUGE_VAL) && errno == ERANGE)
        _errormsg("double value would cause overflow");
    return val;
}

static long _strtolong(char *str)
{
    char *endptr;
    long val = strtol(str, &endptr, 0);

    if (val == 0 && endptr == str)
        _errormsg("error parsing long integer value");
    if ((val == LONG_MIN || val == LONG_MAX) && errno == ERANGE)
        _errormsg("long integer value would cause under/overflow");
    return val;
}

static void _doubletotv(struct timeval *tv, double val)
{
    tv->tv_sec = (val * 10.0)/10; /* crude round-down without -lm */
    tv->tv_usec = ((val - tv->tv_sec) * 1000000.0);
}

static void _errormsg(char *msg)
{
    err_exit(FALSE, "%s: %s::%d", msg, scanner_file(), scanner_line());
}

static void _warnmsg(char *msg)
{
    err(FALSE, "warning: %s: %s::%d", msg, scanner_file(), scanner_line());
}

void yyerror()
{
    _errormsg("parse error");
}

/*
 * vi:tabstop=4 shiftwidth=4 expandtab
 */
